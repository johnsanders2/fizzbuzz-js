export class FizzBuzzItem {
    _number = 0;
    _name = '';

    constructor(number, name) {
        this._number = number;
        this._name = name;
    }

    get number() {
        return this._number;
    }

    set number(value) {
        this._number = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}

export class FizzBuzzGame {
    _rounds = 10;
    _items = [];
    _results = [];

    constructor(rounds, items) {
        this._rounds = rounds;
        this._items = items;
        this._results = [];
    }

    get rounds() {
        return this._rounds;
    }

    set rounds(value) {
        this._rounds = value;

        return this;
    }

    get items() {
        return this._items;
    }

    set items(value) {
        this._items = value;
    }

    get results() {
        return this._results;
    }

    play() {
        const start = 1;
        const end = this._rounds;

        for (let round = start; round <= end; round++) {
            let results = [];

            this._items.forEach(item => {
                if (round % item.number === 0) {
                    results.push(item.name);
                }
            });

            if (results.length === 0) results.push(round);
            this._results.push(results.join('-'));
        }

        return this;
    }
}
